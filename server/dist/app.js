"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const passport_1 = __importDefault(require("passport"));
const passport_2 = __importDefault(require("./middlewares/passport"));
const cors_1 = __importDefault(require("cors"));
const morgan_1 = __importDefault(require("morgan"));
const multer_1 = __importDefault(require("multer"));
const path_1 = __importDefault(require("path"));
const uuid_1 = require("uuid");
const auth_routes_1 = __importDefault(require("./routes/auth.routes"));
const special_routes_1 = __importDefault(require("./routes/special.routes"));
const contactRoutes_1 = __importDefault(require("./routes/contactRoutes"));
const driverRoutes_1 = __importDefault(require("./routes/driverRoutes"));
const adminRoutes_1 = __importDefault(require("./routes/adminRoutes"));
const medicalRoutes_1 = __importDefault(require("./routes/medicalRoutes"));
const surgeryRoutes_1 = __importDefault(require("./routes/surgeryRoutes"));
const transplantRoutes_1 = __importDefault(require("./routes/transplantRoutes"));
const diseaseRoutes_1 = __importDefault(require("./routes/diseaseRoutes"));
const reportRoutes_1 = __importDefault(require("./routes/reportRoutes"));
const vehicleRoutes_1 = __importDefault(require("./routes/vehicleRoutes"));
const app = express_1.default();
const storage = multer_1.default.diskStorage({
    destination: path_1.default.join(__dirname, 'public/images'),
    filename: (req, file, cb) => {
        cb(null, uuid_1.v4() + path_1.default.extname(file.originalname));
    }
});
const upload = multer_1.default({
    storage,
    dest: path_1.default.join(__dirname, 'public/images'),
    fileFilter: (req, file, cb) => {
        const fileTypes = /jpeg|jpg|png|gif/;
        const mimetype = fileTypes.test(file.mimetype);
        const extname = fileTypes.test(path_1.default.extname(file.originalname));
        if (mimetype && extname) {
            return cb(null, true);
        }
        // @ts-ignore
        cb('Error: El archivo debe ser una imagen ');
    }
}).single('image');
// settings
app.set('port', process.env.PORT || 3000);
// middlewares
app.use(morgan_1.default('dev'));
app.use(cors_1.default());
app.use(express_1.default.urlencoded({ extended: true }));
app.use(express_1.default.json());
app.use(express_1.default.static(path_1.default.join(__dirname, 'public')));
app.use(upload);
app.use(passport_1.default.initialize());
passport_1.default.use(passport_2.default);
app.get('/', (req, res) => {
    return res.send(`The API is at http://localhost:${app.get('port')}`);
});
app.use('/api', auth_routes_1.default);
///rutas que necesitan autorización
app.use('/api', special_routes_1.default);
app.use('/api/driver/', driverRoutes_1.default);
app.use('/api/admin/', adminRoutes_1.default);
app.use('/api/report/', reportRoutes_1.default);
app.use('/api/medical/', medicalRoutes_1.default);
app.use('/api/surgery/', surgeryRoutes_1.default);
app.use('/api/transplant/', transplantRoutes_1.default);
app.use('/api/disease/', diseaseRoutes_1.default);
app.use('/api/vehicle/', vehicleRoutes_1.default);
app.use('/api/contact/', contactRoutes_1.default);
exports.default = app;
