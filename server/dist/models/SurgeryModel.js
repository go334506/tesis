"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const SurgerySchema = new mongoose_1.Schema({
    idUser: { type: String, required: true },
    surgery: { type: String, required: true },
    date: { type: String, required: true },
    cares: { type: String },
    createdAt: { type: Date, default: Date.now },
    updatedAt: Date
});
exports.default = mongoose_1.model('Surgery', SurgerySchema);
