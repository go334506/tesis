"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const TransplantSchema = new mongoose_1.Schema({
    idUser: { type: String, required: true },
    transplant: { type: String },
    date: { type: String },
    cares: { type: String },
    createdAt: { type: Date, default: Date.now },
    updatedAt: Date
});
exports.default = mongoose_1.model('Transplant', TransplantSchema);
