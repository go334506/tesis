"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const ReportSchema = new mongoose_1.Schema({
    idUser: { type: String, required: true },
    velocity: { type: Number, required: true },
    metricUnit: { type: String, required: true },
    latitud: { type: Number, required: true },
    longitud: { type: Number, required: true },
    x: { type: String, required: true },
    y: { type: String, required: true },
    z: { type: String, required: true },
    statusReport: { type: String, required: true },
    createdAt: { type: Date, default: Date.now },
    updatedAt: Date
});
exports.default = mongoose_1.model('Report', ReportSchema);
