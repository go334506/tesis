"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const ContactSchema = new mongoose_1.Schema({
    idUser: { type: String, required: true },
    name: { type: String, required: true },
    appat: { type: String, required: true },
    amat: { type: String, },
    phone: { type: Number, required: true },
    email: { type: String, required: true },
    cp: { type: Number },
    calle: { type: String },
    colony: { type: String },
    municipality: { type: String },
    noext: { type: String },
    noint: { type: String },
    createdAt: { type: Date, default: Date.now },
    updatedAt: Date
});
exports.default = mongoose_1.model('Contact', ContactSchema);
