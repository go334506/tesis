"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const FotosVehicleSchema = new mongoose_1.Schema({
    idUser: { type: String, required: true },
    idVehicle: { type: String, required: true },
    url: { type: String },
    createdAt: { type: Date, default: Date.now },
    updatedAt: Date
});
exports.default = mongoose_1.model('FotosVehicle', FotosVehicleSchema);
