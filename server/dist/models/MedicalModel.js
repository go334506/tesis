"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const MedicalSchema = new mongoose_1.Schema({
    idUser: { type: String, required: true, unique: true },
    tipeBlood: { type: String },
    principalDoctor: { type: String },
    numberDoctor: { type: String },
    donor: { type: Boolean, required: true },
    createdAt: { type: Date, default: Date.now },
    updatedAt: Date
});
exports.default = mongoose_1.model('Medical', MedicalSchema);
