"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const VehicleSchema = new mongoose_1.Schema({
    idUser: { type: String },
    enrollment: { type: String },
    brand: { type: String },
    model: { type: String },
    color: { type: String },
    year: { type: Number },
    country: { type: String },
    nEngine: { type: String },
    nChassis: { type: String },
    createdAt: { type: Date, default: Date.now },
    updatedAt: Date
});
exports.default = mongoose_1.model('Vehicle', VehicleSchema);
