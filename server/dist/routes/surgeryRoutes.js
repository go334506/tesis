"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const surgeryController_1 = require("../controllers/surgeryController");
const passport_1 = __importDefault(require("passport"));
class SurgeryRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.post('/create/', passport_1.default.authenticate("jwt", { session: false }), surgeryController_1.surgeryController.createSurgery);
        this.router.put('/edit/:id', passport_1.default.authenticate("jwt", { session: false }), surgeryController_1.surgeryController.editSurgeries);
        this.router.get('/getsurgery/:id', passport_1.default.authenticate("jwt", { session: false }), surgeryController_1.surgeryController.getSurgery);
        this.router.get('/surgeries/', passport_1.default.authenticate("jwt", { session: false }), surgeryController_1.surgeryController.getSurgeries);
        this.router.delete('/delete/:id', passport_1.default.authenticate("jwt", { session: false }), surgeryController_1.surgeryController.deleteSurgery);
    }
}
const surgeryRoutes = new SurgeryRoutes();
exports.default = surgeryRoutes.router;
