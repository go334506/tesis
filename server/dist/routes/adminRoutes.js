"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const adminController_1 = require("../controllers/adminController");
const passport_1 = __importDefault(require("passport"));
class AdminRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/getdrivers/', passport_1.default.authenticate("jwt", { session: false }), adminController_1.adminController.getDrivers);
        this.router.get('/getdriver/:id', passport_1.default.authenticate("jwt", { session: false }), adminController_1.adminController.getDriverId);
        this.router.put('/edit/:id', passport_1.default.authenticate("jwt", { session: false }), adminController_1.adminController.editDriverId);
        this.router.delete('/delete/:id', passport_1.default.authenticate("jwt", { session: false }), adminController_1.adminController.deleteDriverId);
    }
}
const adminRoutes = new AdminRoutes();
exports.default = adminRoutes.router;
