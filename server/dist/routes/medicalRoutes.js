"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const medicalController_1 = require("../controllers/medicalController");
const passport_1 = __importDefault(require("passport"));
class MedicalRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.post('/create/', passport_1.default.authenticate("jwt", { session: false }), medicalController_1.medicalController.createMedical);
        this.router.put('/edit/', passport_1.default.authenticate("jwt", { session: false }), medicalController_1.medicalController.editMedical);
        this.router.get('/getmedical/', passport_1.default.authenticate("jwt", { session: false }), medicalController_1.medicalController.getMedical);
        this.router.get('/getmedicals/', passport_1.default.authenticate("jwt", { session: false }), medicalController_1.medicalController.getMedicals);
    }
}
const medicalRoutes = new MedicalRoutes();
exports.default = medicalRoutes.router;
