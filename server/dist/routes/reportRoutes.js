"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const reportController_1 = require("../controllers/reportController");
const passport_1 = __importDefault(require("passport"));
class ReportRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.post('/create/', passport_1.default.authenticate("jwt", { session: false }), reportController_1.reportController.createReport);
        this.router.get('/getreports/', passport_1.default.authenticate("jwt", { session: false }), reportController_1.reportController.getReports);
        this.router.get('/getreport/:id', passport_1.default.authenticate("jwt", { session: false }), reportController_1.reportController.getReport);
        this.router.put('/updatestatus/:id', passport_1.default.authenticate("jwt", { session: false }), reportController_1.reportController.updateStatusReport);
    }
}
const reportRoutes = new ReportRoutes();
exports.default = reportRoutes.router;
