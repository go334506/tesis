"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const transplantController_1 = require("../controllers/transplantController");
const passport_1 = __importDefault(require("passport"));
class TransplantRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.post('/create/', passport_1.default.authenticate("jwt", { session: false }), transplantController_1.transplantController.createTransplant);
        this.router.put('/edit/:id', passport_1.default.authenticate("jwt", { session: false }), transplantController_1.transplantController.editTransplant);
        this.router.get('/get/:id', passport_1.default.authenticate("jwt", { session: false }), transplantController_1.transplantController.getTransplant);
        this.router.get('/get/', passport_1.default.authenticate("jwt", { session: false }), transplantController_1.transplantController.getTransplants);
        this.router.delete('/delete/:id', passport_1.default.authenticate("jwt", { session: false }), transplantController_1.transplantController.deleteTransplant);
    }
}
const transplantRoutes = new TransplantRoutes();
exports.default = transplantRoutes.router;
