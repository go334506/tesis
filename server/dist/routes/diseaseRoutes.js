"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const diseaseController_1 = require("../controllers/diseaseController");
const passport_1 = __importDefault(require("passport"));
class DiseaseRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.post('/create/', passport_1.default.authenticate("jwt", { session: false }), diseaseController_1.diseaseController.createDisease);
        this.router.put('/edit/:id', passport_1.default.authenticate("jwt", { session: false }), diseaseController_1.diseaseController.editDisease);
        this.router.get('/get/:id', passport_1.default.authenticate("jwt", { session: false }), diseaseController_1.diseaseController.getDisease);
        this.router.get('/get/', passport_1.default.authenticate("jwt", { session: false }), diseaseController_1.diseaseController.getDiseases);
        this.router.delete('/delete/:id', passport_1.default.authenticate("jwt", { session: false }), diseaseController_1.diseaseController.deleteDisease);
    }
}
const diseaseRoutes = new DiseaseRoutes();
exports.default = diseaseRoutes.router;
