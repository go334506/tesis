"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const vehicleController_1 = require("../controllers/vehicleController");
const passport_1 = __importDefault(require("passport"));
class VehicleRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.post('/create/', passport_1.default.authenticate("jwt", { session: false }), vehicleController_1.vehicleController.createVehicle);
        this.router.put('/edit/:id', passport_1.default.authenticate("jwt", { session: false }), vehicleController_1.vehicleController.editVehicle);
        this.router.get('/getvehicle/:id', passport_1.default.authenticate("jwt", { session: false }), vehicleController_1.vehicleController.getVehicle);
        this.router.get('/getvehicles/', passport_1.default.authenticate("jwt", { session: false }), vehicleController_1.vehicleController.getVehicles);
        this.router.delete('/delete/:id', passport_1.default.authenticate("jwt", { session: false }), vehicleController_1.vehicleController.deleteVehicle);
        // fotos
        this.router.post('/uploadfoto/', passport_1.default.authenticate("jwt", { session: false }), vehicleController_1.vehicleController.uploadFotosVehicle);
        this.router.get('/getfotos/:id', passport_1.default.authenticate("jwt", { session: false }), vehicleController_1.vehicleController.getFotosVehicle);
        this.router.delete('/deletefotos/:id', passport_1.default.authenticate("jwt", { session: false }), vehicleController_1.vehicleController.deleteFotosVehicle);
    }
}
const vehicleRoutes = new VehicleRoutes();
exports.default = vehicleRoutes.router;
