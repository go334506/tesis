"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const contactController_1 = require("../controllers/contactController");
const passport_1 = __importDefault(require("passport"));
class ContactRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/getcontacts/', passport_1.default.authenticate("jwt", { session: false }), contactController_1.contactController.getContacts);
        this.router.post('/create/', passport_1.default.authenticate("jwt", { session: false }), contactController_1.contactController.createContact);
        this.router.get('/getcontact/:id', passport_1.default.authenticate("jwt", { session: false }), contactController_1.contactController.getContact);
        this.router.put('/edit/:id', passport_1.default.authenticate("jwt", { session: false }), contactController_1.contactController.editContact);
        this.router.delete('/delete/:id', passport_1.default.authenticate("jwt", { session: false }), contactController_1.contactController.deleteContact);
    }
}
const contactRoutes = new ContactRoutes();
exports.default = contactRoutes.router;
