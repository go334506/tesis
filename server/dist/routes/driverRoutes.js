"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const driverController_1 = require("../controllers/driverController");
const passport_1 = __importDefault(require("passport"));
class DriverRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        //obtiene su id mediante el token del usuario
        this.router.get('/getdriver/', passport_1.default.authenticate("jwt", { session: false }), driverController_1.driverController.getDriver);
        this.router.put('/edit/', passport_1.default.authenticate("jwt", { session: false }), driverController_1.driverController.editDriver);
        this.router.delete('/delete/', passport_1.default.authenticate("jwt", { session: false }), driverController_1.driverController.deleteDriver);
    }
}
const driverRoutes = new DriverRoutes();
exports.default = driverRoutes.router;
