"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.medicalController = void 0;
const MedicalModel_1 = __importDefault(require("../models/MedicalModel"));
class MedicalController {
    createMedical(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            req.body.idUser = userToken.id; // guardarle el id del usuario del token
            const medical = new MedicalModel_1.default(req.body);
            const medicalSelected = yield MedicalModel_1.default.findOne({ idUser: userToken.id });
            if (medicalSelected) {
                console.log('ya existe ');
                res.status(400).json({
                    'status': {
                        'code': 0,
                        'message': "Ya registraste tus datos médicos, editalos por favor."
                    }
                });
            }
            try {
                yield medical.save();
                res.status(200).json({
                    'status': {
                        'code': 1,
                        'message': "Datos médicos de contacto almacenados con éxito"
                    }
                });
            }
            catch (e) {
                res.status(400).json({
                    'status': {
                        'code': 0,
                        'message': "Lo sentimos no se ha podido guardar sus datos médicos."
                    }
                });
            }
            res.send('/create/');
        });
    }
    editMedical(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const medical = yield MedicalModel_1.default.findOne({ idUser: userToken.id });
                const medicalObject = {
                    tipeBlood: req.body.tipeBlood,
                    principalDoctor: req.body.principalDoctor,
                    numberDoctor: req.body.numberDoctor,
                    donor: req.body.donor
                };
                if (medical) {
                    yield MedicalModel_1.default.find({ idUser: { $eq: userToken.id } }).update({ $set: medicalObject });
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Datos médicos actualizados correctamente.'
                        },
                        'content': {}
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Datos médicos no encontrados.'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos pero no se pudo editar sus datos médicos.'
                    },
                    'content': []
                });
            }
        });
    }
    getMedical(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const medical = yield MedicalModel_1.default.findOne({ idUser: userToken.id });
                if (medical) {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Reporte Médico'
                        },
                        'content': medical
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'No hay información médica'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos pero no se cargo correctamente la información médica'
                    },
                    'content': []
                });
            }
        });
    }
    getMedicals(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const medicals = yield MedicalModel_1.default.find({ idUser: userToken.id }, { createdAt: 0, __v: 0 });
                if (medicals.length > 0) {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Lista de datos médicos.'
                        },
                        'content': medicals
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Lo sentimos, pero aún no se han registrado datos médicos.'
                        },
                        'content': []
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos pero no se cargaron correctamente sus contactos de emergencía'
                    },
                    'content': []
                });
            }
        });
    }
}
exports.medicalController = new MedicalController();
