"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.contactController = void 0;
const Contact_1 = __importDefault(require("../models/Contact"));
class ContactController {
    createContact(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            req.body.idUser = userToken.id; // guardarle el id del usuario del token
            const contact = new Contact_1.default(req.body);
            const contactSelect = yield Contact_1.default.find({ idUser: userToken.id, email: req.body.email });
            if (contactSelect.length > 0) {
                console.log("Ya existe ese número de contacto");
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Ya registraste ese correo como contacto de emergencia.'
                    }
                });
                return;
            }
            else {
                try {
                    yield contact.save();
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': "Datos de contacto almacenados con éxito"
                        }
                    });
                }
                catch (e) {
                    console.log(e);
                    res.status(400).json({
                        'status': {
                            'code': 0,
                            'message': "Lo sentimos no se ha podido agregar el contacto de emergencía"
                        }
                    });
                }
            }
        });
    }
    getContacts(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const contacts = yield Contact_1.default.find({ idUser: userToken.id }, { createdAt: 0, __v: 0, idUser: 0 });
                if (contacts.length > 0) {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Lista de Contactos'
                        },
                        'content': contacts
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Lo sentimos pero aún no tienes registrados contactos de emergencía'
                        },
                        'content': []
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos pero no se cargaron correctamente sus contactos de emergencía'
                    },
                    'content': []
                });
            }
        });
    }
    getContact(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const contact = yield Contact_1.default.findOne({ _id: req.params.id, idUser: userToken.id }, { _id: 0, createdAt: 0, __v: 0, idUser: 0 });
                if (contact) {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Información del contacto emergencia'
                        },
                        'content': contact
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Información del contacto de emergencia no encontrada'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos pero no se cargo correctamente la información de su contacto de emergencía'
                    },
                    'content': []
                });
            }
        });
    }
    editContact(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const contact = yield Contact_1.default.findOne({ _id: req.params.id, idUser: userToken.id });
                const contactObject = {
                    name: req.body.name,
                    appat: req.body.appat,
                    amat: req.body.amat,
                    phone: req.body.phone,
                    email: req.body.email,
                    calle: req.body.calle,
                    colony: req.body.colony,
                    municipality: req.body.municipality,
                    noext: req.body.noext,
                    noint: req.body.noint,
                };
                if (contact) {
                    yield Contact_1.default.find({ _id: { $eq: req.params.id } }).update({ $set: contactObject });
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Contacto de emergencia ' + contactObject.name + ' actualizado correctamente'
                        },
                        'content': contact
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Información del contacto de emergencia no encontrada'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos pero no se pudo editar la información de su contacto de emergencía'
                    },
                    'content': []
                });
            }
        });
    }
    deleteContact(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const contact = yield Contact_1.default.findOne({ _id: req.params.id, idUser: userToken.id }, { _id: 0, createdAt: 0, __v: 0, idUser: 0 });
                if (contact) {
                    yield Contact_1.default.findByIdAndRemove(req.params.id);
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Contacto de emergencía elminiado Correctamente'
                        },
                        'content': {}
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Información del contacto de emergencia no encontrada'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos pero no se cargo correctamente la información de su contacto de emergencía'
                    },
                    'content': []
                });
            }
        });
    }
}
exports.contactController = new ContactController();
