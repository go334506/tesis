"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.driverController = void 0;
const User_1 = __importDefault(require("../models/User"));
class DriverController {
    getDriver(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            const user = yield User_1.default.findById(userToken.id, { password: 0 });
            res.status(200).json({
                'status': {
                    'code': 1,
                    'message': 'Información del Usuario'
                },
                'content': user
            });
        });
    }
    editDriver(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            const userSelect = yield User_1.default.findById(userToken.id, { password: 0 });
            const user = {
                name: req.body.name,
                lastname1: req.body.lastname1,
                lastname2: req.body.lastname2,
                phone: req.body.phone,
                age: req.body.age,
                cp: req.body.cp,
                street: req.body.street,
                colony: req.body.colony,
                municipality: req.body.municipality,
                noext: req.body.noext,
                noint: req.body.noint
            };
            if (userSelect) {
                yield User_1.default.findByIdAndUpdate(userToken.id, { $set: user });
                res.status(200).json({
                    'status': {
                        'code': 1,
                        'message': 'Conductor ' + user.name + ' actualizado correctamente'
                    },
                    'content': {}
                });
            }
            else {
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos no se pudo actualizar la información del contacto de emergencia'
                    },
                    'content': {}
                });
            }
        });
    }
    deleteDriver(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            const userSelect = yield User_1.default.findById(userToken.id, { password: 0 });
            const user = {
                active: false,
            };
            if (userSelect) {
                yield User_1.default.findByIdAndUpdate(userToken.id, { $set: user });
                res.status(200).json({
                    'status': {
                        'code': 1,
                        'message': 'Se ha eliminado correctamente'
                    },
                    'content': {}
                });
            }
            else {
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos no se pudo eliminar'
                    },
                    'content': {}
                });
            }
        });
    }
}
exports.driverController = new DriverController();
