"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.surgeryController = void 0;
const SurgeryModel_1 = __importDefault(require("../models/SurgeryModel"));
class SurgeryController {
    createSurgery(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            req.body.idUser = userToken.id; // guardarle el id del usuario del token
            const surgery = new SurgeryModel_1.default(req.body);
            try {
                yield surgery.save();
                res.status(200).json({
                    'status': {
                        'code': 1,
                        'message': "Cirugía almacenada con éxito."
                    }
                });
            }
            catch (e) {
                console.log(e);
                res.status(400).json({
                    'status': {
                        'code': 0,
                        'message': "Lo sentimos no se ha podido guardar la Cirugía."
                    }
                });
            }
        });
    }
    getSurgeries(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const surgeries = yield SurgeryModel_1.default.find({ idUser: userToken.id }, { createdAt: 0, __v: 0, idUser: 0 });
                if (surgeries.length > 0) {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Lista de Cirugías.'
                        },
                        'content': surgeries
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'No tienes registrada ningúna cirugía.'
                        },
                        'content': []
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos, pero no se cargaron correctamente los datos.'
                    },
                    'content': []
                });
            }
        });
    }
    getSurgery(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const surgery = yield SurgeryModel_1.default.findOne({ _id: req.params.id, idUser: userToken.id }, { _id: 0, createdAt: 0, __v: 0, idUser: 0 });
                if (surgery) {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Información de la cirugía.'
                        },
                        'content': surgery
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Información de la cirugía no encontrada'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos, pero no se cargaron correctamente los datos.'
                    },
                    'content': []
                });
            }
        });
    }
    editSurgeries(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const surgery = yield SurgeryModel_1.default.findOne({ _id: req.params.id, idUser: userToken.id });
                const surgeryObject = {
                    surgery: req.body.surgery,
                    date: req.body.date,
                    cares: req.body.cares
                };
                if (surgery) {
                    yield SurgeryModel_1.default.find({ _id: { $eq: req.params.id } }).update({ $set: surgeryObject });
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': surgeryObject.surgery + ' actualizada correctamente.'
                        },
                        'content': {}
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Información de la cirugía no encontrada'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos pero no se pudo editar la cirugía'
                    },
                    'content': []
                });
            }
        });
    }
    deleteSurgery(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const surgery = yield SurgeryModel_1.default.findOne({ _id: req.params.id, idUser: userToken.id }, { _id: 0, createdAt: 0, __v: 0, idUser: 0 });
                if (surgery) {
                    yield SurgeryModel_1.default.findByIdAndRemove(req.params.id);
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Cirugía eliminada Correctamente.'
                        },
                        'content': {}
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Información de la cirugía no encontrada'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos, pero no se pudo eliminar la cirugía.'
                    },
                    'content': []
                });
            }
        });
    }
}
exports.surgeryController = new SurgeryController();
