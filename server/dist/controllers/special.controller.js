"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.special = void 0;
exports.special = (req, res) => {
    const user = req.user;
    return res.json({ msg: `Hey ${user.id}!` });
};
