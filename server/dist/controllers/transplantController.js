"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.transplantController = void 0;
const TransplantModel_1 = __importDefault(require("../models/TransplantModel"));
class TransplantController {
    createTransplant(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            req.body.idUser = userToken.id; // guardarle el id del usuario del token
            const transplant = new TransplantModel_1.default(req.body);
            try {
                yield transplant.save();
                res.status(200).json({
                    'status': {
                        'code': 1,
                        'message': "Transplante almacenado con éxito."
                    }
                });
            }
            catch (e) {
                console.log(e);
                res.status(400).json({
                    'status': {
                        'code': 0,
                        'message': "Lo sentimos no se ha podido guardar el transplante."
                    }
                });
            }
        });
    }
    getTransplants(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const transplants = yield TransplantModel_1.default.find({ idUser: userToken.id }, { createdAt: 0, __v: 0, idUser: 0 });
                if (transplants.length > 0) {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Lista de Transplantes.'
                        },
                        'content': transplants
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'No tienes registrada ningún Transplante.'
                        },
                        'content': []
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos, pero no se cargaron correctamente los datos.'
                    },
                    'content': []
                });
            }
        });
    }
    getTransplant(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const transplant = yield TransplantModel_1.default.findOne({ _id: req.params.id, idUser: userToken.id }, { _id: 0, createdAt: 0, __v: 0, idUser: 0 });
                if (transplant) {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Información del Transplante.'
                        },
                        'content': transplant
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Información del Transplante no encontrada'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos, pero no se cargaron correctamente los datos.'
                    },
                    'content': []
                });
            }
        });
    }
    editTransplant(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const transplant = yield TransplantModel_1.default.findOne({ _id: req.params.id, idUser: userToken.id });
                const transplantObject = {
                    transplant: req.body.transplant,
                    date: req.body.date,
                    cares: req.body.cares
                };
                if (transplant) {
                    yield TransplantModel_1.default.find({ _id: { $eq: req.params.id } }).update({ $set: transplantObject });
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': transplantObject.transplant + ' actualizado correctamente.'
                        },
                        'content': {}
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Información del transplante no encontrada'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos pero no se pudo editar el transplante'
                    },
                    'content': []
                });
            }
        });
    }
    deleteTransplant(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const surgery = yield TransplantModel_1.default.findOne({ _id: req.params.id, idUser: userToken.id }, { _id: 0, createdAt: 0, __v: 0, idUser: 0 });
                if (surgery) {
                    yield TransplantModel_1.default.findByIdAndRemove(req.params.id);
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Transplante eliminado Correctamente.'
                        },
                        'content': {}
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Información del transplante no encontrada'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos, pero no se pudo eliminar el transplante.'
                    },
                    'content': []
                });
            }
        });
    }
}
exports.transplantController = new TransplantController();
