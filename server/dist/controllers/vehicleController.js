"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.vehicleController = void 0;
const Vehicle_1 = __importDefault(require("../models/Vehicle"));
const FotosVehicle_1 = __importDefault(require("../models/FotosVehicle"));
class VehicleController {
    createVehicle(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            req.body.idUser = userToken.id; // guardarle el id del usuario del token
            const vehicle = new Vehicle_1.default(req.body);
            try {
                yield vehicle.save();
                res.status(200).json({
                    'status': {
                        'code': 1,
                        'message': "Vehículo guardado."
                    }
                });
            }
            catch (e) {
                console.log(e);
                res.status(400).json({
                    'status': {
                        'code': 0,
                        'message': "Lo sentimos no se ha podido guardar tu vehículo."
                    }
                });
            }
        });
    }
    editVehicle(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const vehicle = yield Vehicle_1.default.findOne({ idUser: userToken.id, _id: req.params.id });
                const vehicleObject = {
                    enrollment: req.body.enrollment,
                    brand: req.body.brand,
                    model: req.body.model,
                    color: req.body.color,
                    year: req.body.year,
                    country: req.body.country,
                    nEngine: req.body.nEngine,
                    nChassis: req.body.nChassis
                };
                if (vehicle) {
                    yield Vehicle_1.default.find({ idUser: { $eq: userToken.id } }).update({ $set: vehicleObject });
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Vehiculo actualizado correctamente.'
                        },
                        'content': { vehicle }
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Vehiculo no encontrados.'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos pero no se pudo editar los datos del vehiculo.'
                    },
                    'content': {}
                });
            }
        });
    }
    getVehicle(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const vehicle = yield Vehicle_1.default.findOne({ idUser: userToken.id, _id: req.params.id });
                if (vehicle) {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Vehículo.'
                        },
                        'content': vehicle
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'No hay información del vehículo.'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos pero no se cargo correctamente ningún vehículo.'
                    },
                    'content': []
                });
            }
        });
    }
    getVehicles(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const vehicle = yield Vehicle_1.default.find({ idUser: userToken.id }, { createdAt: 0, __v: 0, idUser: 0 });
                if (vehicle.length > 0) {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Lista de Vehículos.'
                        },
                        'content': vehicle
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'No tienes registradn ningún Vehículo.'
                        },
                        'content': []
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(400).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos, pero no se cargaron correctamente los Vehículos.'
                    },
                    'content': []
                });
            }
        });
    }
    deleteVehicle(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const vehicle = yield Vehicle_1.default.find({ idUser: userToken.id }, {});
                if (vehicle) {
                    yield Vehicle_1.default.findByIdAndRemove(req.params.id);
                    yield FotosVehicle_1.default.remove({ idVehicle: req.params.id });
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Vehículo eliminado Correctamente.'
                        },
                        'content': {}
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Información del Vehículo no encontrada.'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos, pero no se pudo eliminar el Vehículo.'
                    },
                    'content': []
                });
            }
        });
    }
    // fotos
    uploadFotosVehicle(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            req.body.idUser = userToken.id; // guardarle el id del usuario del token
            console.log('body', req.body);
            console.log('file: ', req.file);
            try {
                // @ts-ignore
                let { filename } = req.file;
                console.log(filename, '***********************************');
                let url = req.protocol + '://' + req.get('host') + "/images/" + filename;
                console.log('url', url);
                req.body.url = url;
                const foto = new FotosVehicle_1.default(req.body);
                yield foto.save();
                res.status(200).json({
                    'status': {
                        'code': 1,
                        'message': 'Se ha cargado correctamente la imágen'
                    },
                    'content': []
                });
            }
            catch (e) {
                res.status(500).json({
                    status: "error",
                    message: "file to upload image"
                });
            }
        });
    }
    getFotosVehicle(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const fotos = yield FotosVehicle_1.default.find({ idVehicle: req.params.id, idUser: userToken.id }, { createdAt: 0, __v: 0, idUser: 0 });
                if (fotos.length > 0) {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Lista de Fotos.'
                        },
                        'content': fotos
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'No tienes Fotos.'
                        },
                        'content': []
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(400).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos, pero no se cargaron correctamente las fotos.'
                    },
                    'content': []
                });
            }
        });
    }
    deleteFotosVehicle(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const foto = yield FotosVehicle_1.default.find({ idVehicle: req.params.id, idUser: userToken.id, _id: req.params.id }, {});
                if (foto) {
                    yield FotosVehicle_1.default.findByIdAndRemove(req.params.id);
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Foto eliminada Correctamente.'
                        },
                        'content': {}
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Foto no encontrada.'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos, pero no se pudo eliminar la foto.'
                    },
                    'content': []
                });
            }
        });
    }
}
exports.vehicleController = new VehicleController();
