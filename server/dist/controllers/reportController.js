"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reportController = void 0;
const Report_1 = __importDefault(require("../models/Report"));
class ReportController {
    createReport(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            req.body.idUser = userToken.id; // guardarle el id del usuario del token
            req.body.statusReport = 'Enviado';
            const report = new Report_1.default(req.body);
            try {
                yield report.save();
                res.status(200).json({
                    'status': {
                        'code': 1,
                        'message': "Reporte enviado"
                    }
                });
            }
            catch (e) {
                res.status(400).json({
                    'status': {
                        'code': 0,
                        'message': "Lo sentimos no se ha podido guardar el reporte, revise su conexión e intente de nuevo."
                    }
                });
            }
            res.send('/create/');
        });
    }
    getReports(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const reports = yield Report_1.default.find({ idUser: userToken.id }, { __v: 0 });
                if (reports.length > 0) {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Reportes.'
                        },
                        'content': reports
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'No existen Reportes.'
                        },
                        'content': []
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos, pero no se pudieron cargar los reportes, revisa tu conexión e intenta de nuevo.'
                    },
                    'content': []
                });
            }
        });
    }
    getReport(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const report = yield Report_1.default.findOne({ _id: req.params.id, idUser: userToken.id }, { _id: 0, __v: 0, idUser: 0 });
                if (report) {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Reportes.'
                        },
                        'content': report
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Reporte no .'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos, pero no se cargaron correctamente los repotes.'
                    },
                    'content': []
                });
            }
        });
    }
    updateStatusReport(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const report = yield Report_1.default.findOne({ _id: req.params.id, idUser: userToken.id });
                const reportObject = {
                    statusReport: req.body.statusReport
                };
                if (report) {
                    yield Report_1.default.find({ _id: { $eq: req.params.id } }).update({ $set: reportObject });
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Se ha actualizado el status del reporte correctamente.'
                        },
                        'content': {}
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Información del reporte no encontrada.'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos pero no se pudo actualizar el estatus del reporte.'
                    },
                    'content': []
                });
            }
        });
    }
}
exports.reportController = new ReportController();
