"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.diseaseController = void 0;
const DiseaseModel_1 = __importDefault(require("../models/DiseaseModel"));
class DiseaseController {
    createDisease(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            req.body.idUser = userToken.id; // guardarle el id del usuario del token
            const disease = new DiseaseModel_1.default(req.body);
            try {
                yield disease.save();
                res.status(200).json({
                    'status': {
                        'code': 1,
                        'message': "Enfermedad almacenada con éxito."
                    }
                });
            }
            catch (e) {
                console.log(e);
                res.status(400).json({
                    'status': {
                        'code': 0,
                        'message': "Lo sentimos no se ha podido guardar la Enfermedad."
                    }
                });
            }
        });
    }
    getDiseases(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const disease = yield DiseaseModel_1.default.find({ idUser: userToken.id }, { createdAt: 0, __v: 0, idUser: 0 });
                if (disease.length > 0) {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Lista de Enfermedades.'
                        },
                        'content': disease
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'No tienes registrada ningúna Enfermedad.'
                        },
                        'content': []
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos, pero no se cargaron correctamente los datos.'
                    },
                    'content': []
                });
            }
        });
    }
    getDisease(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const disease = yield DiseaseModel_1.default.findOne({ _id: req.params.id, idUser: userToken.id }, { _id: 0, createdAt: 0, __v: 0, idUser: 0 });
                if (disease) {
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Información de la enfermedad.'
                        },
                        'content': disease
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Información de la enfermedad no encontrada'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos, pero no se cargaron correctamente los datos.'
                    },
                    'content': []
                });
            }
        });
    }
    editDisease(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const disease = yield DiseaseModel_1.default.findOne({ _id: req.params.id, idUser: userToken.id });
                const diseaseObject = {
                    disease: req.body.disease,
                    treatment: req.body.treatment
                };
                if (disease) {
                    yield DiseaseModel_1.default.find({ _id: { $eq: req.params.id } }).update({ $set: diseaseObject });
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': diseaseObject.disease + ' actualizado correctamente.'
                        },
                        'content': {}
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Información de la enfermedad no encontrada'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos pero no se pudo editar la enfermedad'
                    },
                    'content': []
                });
            }
        });
    }
    deleteDisease(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userToken = req.user;
            try {
                const surgery = yield DiseaseModel_1.default.findOne({ _id: req.params.id, idUser: userToken.id }, { _id: 0, createdAt: 0, __v: 0, idUser: 0 });
                if (surgery) {
                    yield DiseaseModel_1.default.findByIdAndRemove(req.params.id);
                    res.status(200).json({
                        'status': {
                            'code': 1,
                            'message': 'Enfermedad eliminada Correctamente.'
                        },
                        'content': {}
                    });
                }
                else {
                    res.status(200).json({
                        'status': {
                            'code': 0,
                            'message': 'Información de la enfermedad no encontrada'
                        },
                        'content': {}
                    });
                }
            }
            catch (e) {
                console.log(e);
                res.status(200).json({
                    'status': {
                        'code': 0,
                        'message': 'Lo sentimos, pero no se pudo eliminar la enfermedad.'
                    },
                    'content': []
                });
            }
        });
    }
}
exports.diseaseController = new DiseaseController();
