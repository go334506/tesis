import {Request, Response} from "express";
import Disease from "../models/DiseaseModel";
import Vehicle from "../models/Vehicle";
import FotosVehicle from "../models/FotosVehicle";
import Medical from "../models/MedicalModel";
import Surgery from "../models/SurgeryModel";

class VehicleController{


    public async createVehicle(req: Request,res: Response){
        const userToken:any = req.user;
        req.body.idUser = userToken.id;// guardarle el id del usuario del token

        const vehicle  =new Vehicle(req.body);


        try {
            await vehicle.save();
            res.status(200).json({
                'status':{
                    'code':1,
                    'message':"Vehículo guardado."
                }

            });
        }catch (e) {
            console.log(e);
            res.status(400).json({
                'status':{
                    'code':0,
                    'message':"Lo sentimos no se ha podido guardar tu vehículo."
                }

            });
        }

    }
    public async editVehicle(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const vehicle = await Vehicle.findOne({idUser: userToken.id, _id: req.params.id});

            const vehicleObject ={
                enrollment: req.body.enrollment,
                brand: req.body.brand,
                model: req.body.model,
                color: req.body.color,
                year: req.body.year,
                country: req.body.country,
                nEngine: req.body.nEngine,
                nChassis: req.body.nChassis

            };


            if (vehicle){
                await Vehicle.find( {idUser: { $eq: userToken.id }}).update({ $set: vehicleObject});

                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Vehiculo actualizado correctamente.'
                    },
                    'content': {vehicle}
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Vehiculo no encontrados.'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos pero no se pudo editar los datos del vehiculo.'
                },
                'content':{}
            });
        }

    }

    public async getVehicle(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const vehicle = await Vehicle.findOne({idUser: userToken.id, _id: req.params.id});
            if (vehicle){
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Vehículo.'
                    },
                    'content': vehicle
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'No hay información del vehículo.'
                    },
                    'content': {}
                });

            }

        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos pero no se cargo correctamente ningún vehículo.'
                },
                'content':[]
            });
        }

    }

    public async getVehicles(req: Request,res: Response){

        const userToken:any = req.user;
        try {
            const vehicle = await Vehicle.find({idUser:userToken.id},{createdAt:0,__v:0,idUser:0});

            if (vehicle.length>0){
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Lista de Vehículos.'
                    },
                    'content':vehicle
                });
            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'No tienes registradn ningún Vehículo.'
                    },
                    'content':[]
                });
            }
        }catch (e) {
            console.log(e);
            res.status(400).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos, pero no se cargaron correctamente los Vehículos.'
                },
                'content':[]
            });
        }

    }

    public async deleteVehicle(req: Request, res: Response){
        const userToken:any = req.user;
        try {
            const vehicle = await Vehicle.find({idUser:userToken.id},{});
            if (vehicle){
                await Vehicle.findByIdAndRemove(req.params.id);
                await  FotosVehicle.remove({idVehicle :req.params.id});

                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Vehículo eliminado Correctamente.'
                    },
                    'content': {}
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Información del Vehículo no encontrada.'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos, pero no se pudo eliminar el Vehículo.'
                },
                'content':[]
            });
        }


    }

    // fotos
    public async uploadFotosVehicle(req: any,res: Response){
        const userToken:any = req.user;
        req.body.idUser = userToken.id;// guardarle el id del usuario del token

        console.log('body', req.body);
        console.log('file: ', req.file);


        try{

            // @ts-ignore
            let {filename} = req.file;
            console.log(filename,'***********************************');
            let url = req.protocol + '://' + req.get('host') + "/images/" + filename

            console.log('url', url);

            req.body.url = url;

            const foto  =new FotosVehicle(req.body);

            await foto.save();

            res.status(200).json({
                'status':{
                    'code':1,
                    'message':'Se ha cargado correctamente la imágen'
                },
                'content':[]
            });
        }catch (e) {
            res.status(500).json({
                status:"error",
                message:"file to upload image"
            })
        }




    }

    public async getFotosVehicle(req: Request,res: Response){

        const userToken:any = req.user;
        try {
            const fotos = await FotosVehicle.find({idVehicle: req.params.id,idUser: userToken.id},{createdAt:0,__v:0,idUser:0});

            if (fotos.length>0){
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Lista de Fotos.'
                    },
                    'content':fotos
                });
            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'No tienes Fotos.'
                    },
                    'content':[]
                });
            }
        }catch (e) {
            console.log(e);
            res.status(400).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos, pero no se cargaron correctamente las fotos.'
                },
                'content':[]
            });
        }

    }

    public async deleteFotosVehicle(req: Request, res: Response){
        const userToken:any = req.user;
        try {
            const foto = await FotosVehicle.find({idVehicle: req.params.id,idUser: userToken.id, _id: req.params.id},{});
            if (foto){
                await FotosVehicle.findByIdAndRemove(req.params.id);

                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Foto eliminada Correctamente.'
                    },
                    'content': {}
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Foto no encontrada.'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos, pero no se pudo eliminar la foto.'
                },
                'content':[]
            });
        }


    }




}

export const vehicleController = new VehicleController();
