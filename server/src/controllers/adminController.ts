import {request, Request, Response} from "express";
import User, { IUser } from "../models/User";

class AdminController{

    public async getDrivers(req: Request,res: Response){
        const userToken:any = req.user;
        if (userToken.role!="admin"){
            res.status(403).json({
                'status':{
                    'code':0,
                    'message':'El cliente no posee los permisos necesarios para cierto contenido'
                },
                'content':{}
            });
        }
        const  user = await User.find({role:"driver"},{password:0});

        res.status(200).json({
            'status':{
                'code':1,
                'message':'Lista de Usuarios'
            },
            'content':user
        });


    }

    public async getDriver(req: Request,res: Response){
        const userToken:any = req.user;
        if (userToken.role!="admin"){
            res.status(403).json({
                'status':{
                    'code':0,
                    'message':'El cliente no posee los permisos necesarios para cierto contenido'
                },
                'content':{}
            });
        }
        const user = await User.findById(userToken.id,{password:0});


        res.status(200).json({
            'status':{
                'code':1,
                'message':'Información del Usuario'
            },
            'content':user
        });


    }

    public async getDriverId(req: Request,res: Response){
        const userToken:any = req.user;
        if (userToken.role!="admin"){
            res.status(403).json({
                'status':{
                    'code':0,
                    'message':'El cliente no posee los permisos necesarios para cierto contenido'
                },
                'content':{}
            });
        }
        if (userToken.role!="admin"){ // si la peticion no la hace un administrador
            res.status(403).json({
                'status':{
                    'code':0,
                    'message':'El cliente no posee los permisos necesarios para cierto contenido'
                },
                'content':{}
            });
        }
        const user = await User.findById(req.params.id,{password:0});

        res.status(200).json({
            'status':{
                'code':1,
                'message':'Información del Usuario'
            },
            'content':user
        });


    }

    public async editDriverId(req: Request,res: Response){
        const userToken:any = req.user;
        if (userToken.role!="admin"){
            res.status(403).json({
                'status':{
                    'code':0,
                    'message':'El cliente no posee los permisos necesarios para cierto contenido'
                },
                'content':{}
            });
        }
        const { id } = req.params;

        const userSelect = await User.findById(id,{password:0});

        const user = {
            name: req.body.name,
            lastname1: req.body.lastname1,
            lastname2: req.body.lastname2,
            phone: req.body.phone,
            age: req.body.age,
            cp: req.body.cp,
            street: req.body.street,
            colony: req.body.colony,
            municipality: req.body.municipality,
            noext: req.body.noext,
            noint: req.body.noint
        }

        if(userSelect){
            await User.findByIdAndUpdate(id,{$set:user});
            res.status(200).json({
                'status':{
                    'code':1,
                    'message':'Conductor '+user.name+' actualizado correctamente'
                },
                'content':{}
            });
        }else {
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos no se pudo actualizar la información del contacto de emergencia'
                },
                'content':{}
            });
        }

    }

    public async deleteDriverId(req: Request, res: Response){
        const userToken:any = req.user;
        if (userToken.role!="admin"){
            res.status(403).json({
                'status':{
                    'code':0,
                    'message':'El cliente no posee los permisos necesarios para cierto contenido'
                },
                'content':{}
            });
        }
        const { id } = req.params;

        const userSelect = await User.findById(id,{password:0});

        const user = {
            active: false,
        }

        if(userSelect){
            await User.findByIdAndUpdate(id,{$set:user});
            res.status(200).json({
                'status':{
                    'code':1,
                    'message':'Se ha eliminado correctamente'
                },
                'content':{}
            });
        }else {
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos no se pudo eliminar'
                },
                'content':{}
            });
        }

    }

}

export const adminController = new AdminController();
