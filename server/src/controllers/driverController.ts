import {request, Request, Response} from "express";
import User, { IUser } from "../models/User";

class DriverController{


    public async getDriver(req: Request,res: Response){
        const userToken:any = req.user;
        const user = await User.findById(userToken.id,{password:0});


        res.status(200).json({
            'status':{
                'code':1,
                'message':'Información del Usuario'
            },
            'content':user
        });


    }

    public async editDriver(req: Request,res: Response){
        const userToken:any = req.user;
        const userSelect = await User.findById(userToken.id,{password:0});

        const user = {
            name: req.body.name,
            lastname1: req.body.lastname1,
            lastname2: req.body.lastname2,
            phone: req.body.phone,
            age: req.body.age,
            cp: req.body.cp,
            street: req.body.street,
            colony: req.body.colony,
            municipality: req.body.municipality,
            noext: req.body.noext,
            noint: req.body.noint
        }

        if(userSelect){
            await User.findByIdAndUpdate(userToken.id,{$set:user});
            res.status(200).json({
                'status':{
                    'code':1,
                    'message':'Conductor '+user.name+' actualizado correctamente'
                },
                'content':{}
            });
        }else {
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos no se pudo actualizar la información del contacto de emergencia'
                },
                'content':{}
            });
        }

    }

    public async deleteDriver(req: Request, res: Response){
        const userToken:any = req.user;

        const userSelect = await User.findById(userToken.id,{password:0});

        const user = {
            active: false,
        }

        if(userSelect){
            await User.findByIdAndUpdate(userToken.id,{$set:user});
            res.status(200).json({
                'status':{
                    'code':1,
                    'message':'Se ha eliminado correctamente'
                },
                'content':{}
            });
        }else {
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos no se pudo eliminar'
                },
                'content':{}
            });
        }

    }

}

export const driverController = new DriverController();
