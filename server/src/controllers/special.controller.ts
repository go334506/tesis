import {Request, Response} from 'express'

export const special = (req: Request, res: Response) => {
    const user:any = req.user;
    return res.json({ msg: `Hey ${user.id}!` });
};
