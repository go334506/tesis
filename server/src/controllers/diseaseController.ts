import {Request, Response} from "express";
import Disease from "../models/DiseaseModel";

class DiseaseController{

    public async createDisease(req: Request,res: Response){
        const userToken:any = req.user;
        req.body.idUser = userToken.id;// guardarle el id del usuario del token

        const disease  =new Disease(req.body);


       try {
            await disease.save();
            res.status(200).json({
                'status':{
                    'code':1,
                    'message':"Enfermedad almacenada con éxito."
                }

            });
        }catch (e) {
            console.log(e);
           res.status(400).json({
               'status':{
                   'code':0,
                   'message':"Lo sentimos no se ha podido guardar la Enfermedad."
               }

           });
        }




    }

    public async getDiseases(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const disease = await Disease.find({idUser:userToken.id},{createdAt:0,__v:0,idUser:0});

            if (disease.length>0){
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Lista de Enfermedades.'
                    },
                    'content':disease
                });
            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'No tienes registrada ningúna Enfermedad.'
                    },
                    'content':[]
                });
            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos, pero no se cargaron correctamente los datos.'
                },
                'content':[]
            });
        }


    }

    public async getDisease(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const disease = await Disease.findOne({_id:req.params.id, idUser: userToken.id},{_id:0,createdAt:0,__v:0,idUser:0});
            if (disease){
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Información de la enfermedad.'
                    },
                    'content':disease
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Información de la enfermedad no encontrada'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos, pero no se cargaron correctamente los datos.'
                },
                'content':[]
            });
        }


    }

    public async editDisease(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const disease = await Disease.findOne({_id:req.params.id, idUser: userToken.id});

            const diseaseObject ={
                disease: req.body.disease,
                treatment: req.body.treatment
            };


            if (disease){

                await Disease.find( {_id: { $eq: req.params.id }}).update({ $set: diseaseObject});

                res.status(200).json({
                    'status':{
                        'code':1,
                        'message': diseaseObject.disease+ ' actualizado correctamente.'
                    },
                    'content': {}
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Información de la enfermedad no encontrada'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos pero no se pudo editar la enfermedad'
                },
                'content':[]
            });
        }
    }

    public async deleteDisease(req: Request, res: Response){
        const userToken:any = req.user;
        try {
            const surgery = await Disease.findOne({_id:req.params.id, idUser: userToken.id},{_id:0,createdAt:0,__v:0,idUser:0});
            if (surgery){
                await Disease.findByIdAndRemove(req.params.id);
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Enfermedad eliminada Correctamente.'
                    },
                    'content': {}
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Información de la enfermedad no encontrada'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos, pero no se pudo eliminar la enfermedad.'
                },
                'content':[]
            });
        }


    }
}

export const diseaseController = new DiseaseController();
