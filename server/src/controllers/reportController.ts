import {Request, Response} from "express";
import Report from "../models/Report";
import Disease from "../models/DiseaseModel";

class ReportController{

    public async createReport(req: Request,res: Response){
        const userToken:any = req.user;
        req.body.idUser = userToken.id;// guardarle el id del usuario del token
        req.body.statusReport = 'Enviado';
        const report  =new Report(req.body);

        try {
            await report.save();
            res.status(200).json({
                'status':{
                    'code':1,
                    'message':"Reporte enviado"
                }


            });
        }catch(e){
            res.status(400).json({
                'status':{
                    'code':0,
                    'message':"Lo sentimos no se ha podido guardar el reporte, revise su conexión e intente de nuevo."
                }

            });

        }
        res.send('/create/')

    }

    public async getReports(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const reports = await Report.find({idUser: userToken.id},{__v:0});

            if (reports.length>0){
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Reportes.'
                    },
                    'content':reports
                });
            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'No existen Reportes.'
                    },
                    'content':[]
                });
            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos, pero no se pudieron cargar los reportes, revisa tu conexión e intenta de nuevo.'
                },
                'content':[]
            });
        }

    }
    public async getReport(req: Request,res: Response) {
        const userToken:any = req.user;
        try {
            const report = await Report.findOne({_id:req.params.id, idUser: userToken.id},{_id:0,__v:0,idUser:0});
            if (report){
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Reportes.'
                    },
                    'content':report
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Reporte no .'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos, pero no se cargaron correctamente los repotes.'
                },
                'content':[]
            });
        }

    }

    public async updateStatusReport(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const report = await Report.findOne({_id:req.params.id, idUser: userToken.id});

            const reportObject ={
                statusReport: req.body.statusReport
            };


            if (report){

                await Report.find( {_id: { $eq: req.params.id }}).update({ $set: reportObject});

                res.status(200).json({
                    'status':{
                        'code':1,
                        'message': 'Se ha actualizado el status del reporte correctamente.'
                    },
                    'content': {}
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Información del reporte no encontrada.'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos pero no se pudo actualizar el estatus del reporte.'
                },
                'content':[]
            });
        }

    }


}

export const reportController = new ReportController();
