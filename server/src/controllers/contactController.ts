import {Request, Response} from "express";
import Contact from "../models/Contact";

class ContactController{

    public async createContact(req: Request,res: Response){
        const userToken:any = req.user;
        req.body.idUser = userToken.id;// guardarle el id del usuario del token

        const contact  =new Contact(req.body);
        const contactSelect = await Contact.find({idUser:userToken.id,email: req.body.email},);


        if(contactSelect.length>0){
            console.log("Ya existe ese número de contacto");
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Ya registraste ese correo como contacto de emergencia.'
                }
            });
            return ;
        }else{
           try {
                await contact.save();
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':"Datos de contacto almacenados con éxito"
                    }

                });
            }catch (e) {
                console.log(e);
               res.status(400).json({
                   'status':{
                       'code':0,
                       'message':"Lo sentimos no se ha podido agregar el contacto de emergencía"
                   }

               });
            }

        }




    }

    public async getContacts(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const contacts = await Contact.find({idUser:userToken.id},{createdAt:0,__v:0,idUser:0});

            if (contacts.length>0){
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Lista de Contactos'
                    },
                    'content':contacts
                });
            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Lo sentimos pero aún no tienes registrados contactos de emergencía'
                    },
                    'content':[]
                });
            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos pero no se cargaron correctamente sus contactos de emergencía'
                },
                'content':[]
            });
        }


    }

    public async getContact(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const contact = await Contact.findOne({_id:req.params.id, idUser: userToken.id},{_id:0,createdAt:0,__v:0,idUser:0});
            if (contact){
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Información del contacto emergencia'
                    },
                    'content':contact
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Información del contacto de emergencia no encontrada'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos pero no se cargo correctamente la información de su contacto de emergencía'
                },
                'content':[]
            });
        }


    }

    public async editContact(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const contact = await Contact.findOne({_id:req.params.id, idUser: userToken.id});

            const contactObject ={
                name: req.body.name,
                appat: req.body.appat,
                amat: req.body.amat,
                phone: req.body.phone,
                email: req.body.email,
                calle: req.body.calle,
                colony: req.body.colony,
                municipality: req.body.municipality,
                noext: req.body.noext,
                noint: req.body.noint,
            };


            if (contact){

                await Contact.find( {_id: { $eq: req.params.id }}).update({ $set: contactObject});

                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Contacto de emergencia '+ contactObject.name+ ' actualizado correctamente'
                    },
                    'content':contact
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Información del contacto de emergencia no encontrada'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos pero no se pudo editar la información de su contacto de emergencía'
                },
                'content':[]
            });
        }
    }

    public async deleteContact(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const contact = await Contact.findOne({_id:req.params.id, idUser: userToken.id},{_id:0,createdAt:0,__v:0,idUser:0});
            if (contact){
                await Contact.findByIdAndRemove(req.params.id);
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Contacto de emergencía elminiado Correctamente'
                    },
                    'content': {}
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Información del contacto de emergencia no encontrada'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos pero no se cargo correctamente la información de su contacto de emergencía'
                },
                'content':[]
            });
        }


    }
}

export const contactController = new ContactController();
