import {Request, Response} from "express";
import Surgery from "../models/SurgeryModel";

class SurgeryController{

    public async createSurgery(req: Request,res: Response){
        const userToken:any = req.user;
        req.body.idUser = userToken.id;// guardarle el id del usuario del token

        const surgery  =new Surgery(req.body);


       try {
            await surgery.save();
            res.status(200).json({
                'status':{
                    'code':1,
                    'message':"Cirugía almacenada con éxito."
                }

            });
        }catch (e) {
            console.log(e);
           res.status(400).json({
               'status':{
                   'code':0,
                   'message':"Lo sentimos no se ha podido guardar la Cirugía."
               }

           });
        }






    }

    public async getSurgeries(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const surgeries = await Surgery.find({idUser:userToken.id},{createdAt:0,__v:0,idUser:0});

            if (surgeries.length>0){
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Lista de Cirugías.'
                    },
                    'content':surgeries
                });
            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'No tienes registrada ningúna cirugía.'
                    },
                    'content':[]
                });
            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos, pero no se cargaron correctamente los datos.'
                },
                'content':[]
            });
        }


    }

    public async getSurgery(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const surgery = await Surgery.findOne({_id:req.params.id, idUser: userToken.id},{_id:0,createdAt:0,__v:0,idUser:0});
            if (surgery){
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Información de la cirugía.'
                    },
                    'content':surgery
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Información de la cirugía no encontrada'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos, pero no se cargaron correctamente los datos.'
                },
                'content':[]
            });
        }


    }

    public async editSurgeries(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const surgery = await Surgery.findOne({_id:req.params.id, idUser: userToken.id});

            const surgeryObject ={
                surgery: req.body.surgery,
                date: req.body.date,
                cares: req.body.cares
            };


            if (surgery){

                await Surgery.find( {_id: { $eq: req.params.id }}).update({ $set: surgeryObject});

                res.status(200).json({
                    'status':{
                        'code':1,
                        'message': surgeryObject.surgery+ ' actualizada correctamente.'
                    },
                    'content': {}
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Información de la cirugía no encontrada'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos pero no se pudo editar la cirugía'
                },
                'content':[]
            });
        }
    }

    public async deleteSurgery(req: Request, res: Response){
        const userToken:any = req.user;
        try {
            const surgery = await Surgery.findOne({_id:req.params.id, idUser: userToken.id},{_id:0,createdAt:0,__v:0,idUser:0});
            if (surgery){
                await Surgery.findByIdAndRemove(req.params.id);
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Cirugía eliminada Correctamente.'
                    },
                    'content': {}
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Información de la cirugía no encontrada'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos, pero no se pudo eliminar la cirugía.'
                },
                'content':[]
            });
        }


    }
}

export const surgeryController = new SurgeryController();
