import {Request, Response} from "express";
import Transplant from "../models/TransplantModel";

class TransplantController{

    public async createTransplant(req: Request,res: Response){
        const userToken:any = req.user;
        req.body.idUser = userToken.id;// guardarle el id del usuario del token

        const transplant  =new Transplant(req.body);


       try {
            await transplant.save();
            res.status(200).json({
                'status':{
                    'code':1,
                    'message':"Transplante almacenado con éxito."
                }

            });
        }catch (e) {
            console.log(e);
           res.status(400).json({
               'status':{
                   'code':0,
                   'message':"Lo sentimos no se ha podido guardar el transplante."
               }

           });
        }






    }

    public async getTransplants(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const transplants = await Transplant.find({idUser:userToken.id},{createdAt:0,__v:0,idUser:0});

            if (transplants.length>0){
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Lista de Transplantes.'
                    },
                    'content':transplants
                });
            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'No tienes registrada ningún Transplante.'
                    },
                    'content':[]
                });
            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos, pero no se cargaron correctamente los datos.'
                },
                'content':[]
            });
        }


    }

    public async getTransplant(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const transplant = await Transplant.findOne({_id:req.params.id, idUser: userToken.id},{_id:0,createdAt:0,__v:0,idUser:0});
            if (transplant){
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Información del Transplante.'
                    },
                    'content':transplant
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Información del Transplante no encontrada'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos, pero no se cargaron correctamente los datos.'
                },
                'content':[]
            });
        }


    }

    public async editTransplant(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const transplant = await Transplant.findOne({_id:req.params.id, idUser: userToken.id});

            const transplantObject ={
                transplant: req.body.transplant,
                date: req.body.date,
                cares: req.body.cares
            };


            if (transplant){

                await Transplant.find( {_id: { $eq: req.params.id }}).update({ $set: transplantObject});

                res.status(200).json({
                    'status':{
                        'code':1,
                        'message': transplantObject.transplant+ ' actualizado correctamente.'
                    },
                    'content': {}
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Información del transplante no encontrada'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos pero no se pudo editar el transplante'
                },
                'content':[]
            });
        }
    }

    public async deleteTransplant(req: Request, res: Response){
        const userToken:any = req.user;
        try {
            const surgery = await Transplant.findOne({_id:req.params.id, idUser: userToken.id},{_id:0,createdAt:0,__v:0,idUser:0});
            if (surgery){
                await Transplant.findByIdAndRemove(req.params.id);
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Transplante eliminado Correctamente.'
                    },
                    'content': {}
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Información del transplante no encontrada'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos, pero no se pudo eliminar el transplante.'
                },
                'content':[]
            });
        }


    }
}

export const transplantController = new TransplantController();
