import {Request, Response} from "express";
import Medical from "../models/MedicalModel";

class MedicalController{

    public async createMedical(req: Request,res: Response){
        const userToken:any = req.user;
        req.body.idUser = userToken.id;// guardarle el id del usuario del token

        const medical  =new Medical(req.body);

        const medicalSelected = await Medical.findOne({ idUser: userToken.id});
        if (medicalSelected){
            console.log('ya existe ');
            res.status(400).json({
                'status':{
                    'code':0,
                    'message':"Ya registraste tus datos médicos, editalos por favor."
                }

            });
        }

        try {
            await medical.save();
            res.status(200).json({
                'status':{
                    'code':1,
                    'message':"Datos médicos de contacto almacenados con éxito"
                }

            });
        }catch (e) {
            res.status(400).json({
                'status':{
                    'code':0,
                    'message':"Lo sentimos no se ha podido guardar sus datos médicos."
                }

            });
        }
        res.send('/create/')

    }

    public async editMedical(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const medical = await Medical.findOne({ idUser: userToken.id});

            const medicalObject ={
                tipeBlood: req.body.tipeBlood,
                principalDoctor: req.body.principalDoctor,
                numberDoctor: req.body.numberDoctor,
                donor: req.body.donor
            };


            if (medical){
                await Medical.find( {idUser: { $eq: userToken.id }}).update({ $set: medicalObject});

                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Datos médicos actualizados correctamente.'
                    },
                    'content': {}
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Datos médicos no encontrados.'
                    },
                    'content':{}
                });

            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos pero no se pudo editar sus datos médicos.'
                },
                'content':[]
            });
        }

    }

    public async getMedical(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const medical = await Medical.findOne({idUser: userToken.id});
            if (medical){
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Reporte Médico'
                    },
                    'content': medical
                });

            }else{
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'No hay información médica'
                    },
                    'content': {}
                });

            }

        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos pero no se cargo correctamente la información médica'
                },
                'content':[]
            });
        }

    }

    public async getMedicals(req: Request,res: Response){
        const userToken:any = req.user;
        try {
            const medicals = await Medical.find({idUser: userToken.id},{createdAt:0,__v:0});

            if (medicals.length>0){
                res.status(200).json({
                    'status':{
                        'code':1,
                        'message':'Lista de datos médicos.'
                    },
                    'content':medicals
                });
            }else{
                res.status(200).json({
                    'status':{
                        'code':0,
                        'message':'Lo sentimos, pero aún no se han registrado datos médicos.'
                    },
                    'content':[]
                });
            }
        }catch (e) {
            console.log(e);
            res.status(200).json({
                'status':{
                    'code':0,
                    'message':'Lo sentimos pero no se cargaron correctamente sus contactos de emergencía'
                },
                'content':[]
            });
        }


    }


}

export const medicalController = new MedicalController();
