import {Schema, model} from "mongoose";

const DiseaseSchema = new Schema({
    idUser: {type: String, required:true},

    disease: {type: String},
    treatment: {type: String},

    createdAt: { type: Date, default: Date.now},
    updatedAt: Date
})

export default model('Disease',DiseaseSchema);
