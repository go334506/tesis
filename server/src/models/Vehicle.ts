import {Schema, model} from "mongoose";

const VehicleSchema = new Schema({
    idUser: {type: String},
    enrollment: {type: String},//Matrícula
    brand: {type: String},
    model: {type: String},
    color: {type: String},
    year: {type: Number},
    country: {type: String},
    nEngine: {type: String},
    nChassis: {type: String},


    createdAt: { type: Date, default: Date.now},
    updatedAt: Date
})

export default model('Vehicle',VehicleSchema);
