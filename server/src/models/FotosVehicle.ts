import {Schema, model} from "mongoose";

const FotosVehicleSchema = new Schema({
    idUser: {type: String, required:true},
    idVehicle: {type: String, required:true},
    url: {type: String},// foto



    createdAt: { type: Date, default: Date.now},
    updatedAt: Date
})

export default model('FotosVehicle',FotosVehicleSchema);
