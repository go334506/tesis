import {Schema, model} from "mongoose";

const SurgerySchema = new Schema({
    idUser: {type: String, required:true},
    surgery: {type: String, required: true},
    date: {type: String, required: true}, // dia, mes y año
    cares: {type: String},

    createdAt: { type: Date, default: Date.now},
    updatedAt: Date
})

export default model('Surgery',SurgerySchema);
