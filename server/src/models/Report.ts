import {Schema, model} from "mongoose";

const ReportSchema = new Schema({
    idUser: {type: String, required:true},
    velocity: {type: Number, required:true},
    metricUnit: {type: String, required:true},
    latitud: {type: Number, required:true},
    longitud: {type: Number, required:true},
    x: {type: String, required:true},
    y: {type: String, required:true},
    z: {type: String, required:true},
    statusReport: {type: String, required:true},//status del reporte

    createdAt: { type: Date, default: Date.now},
    updatedAt: Date
})

export default model('Report',ReportSchema);
