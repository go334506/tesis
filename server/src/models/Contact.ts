import {Schema, model} from "mongoose";

const ContactSchema = new Schema({
    idUser: {type: String, required:true},
    name: {type: String,required: true},
    appat: {type: String,required: true},
    amat: {type: String,},
    phone: {type: Number, required: true},
    email: {type: String, required: true},
    cp: {type: Number},
    calle: {type: String},
    colony: {type: String},
    municipality: {type: String},
    noext: {type: String},
    noint: {type: String},

    createdAt: { type: Date, default: Date.now},
    updatedAt: Date
})

export default model('Contact',ContactSchema);
