import { model, Schema, Document } from "mongoose";
import bcrypt from "bcrypt";




export interface IUser extends Document {
    email: string;
    password: string;
    name: string;
    lastname1: string;
    lastname2:string;
    phone:number;
    age: number;
    cp: number;
    street:string;
    colony: string;
    municipality: string;
    noext:string;
    noint: string;
    role:string;
    active:boolean;

    comparePassword: (password: string) => Promise<Boolean>
};

const userSchema = new Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        lowercase: true,
        trim: true
    },
    password: {
        type: String,
        required: true
    },
    name: {type: String,
        required:true
    },
    lastname1: {type: String,
        required:true
    },
    lastname2: {type: String,
        required:false
    },
    phone: {type: Number,
        required:true
    },
    age: {type: Number,
        required:true
    },
    cp: {type: Number,
        required:true
    },
    street: {type: String,
        required:true
    },
    colony: {type: String,
        required:true
    },
    municipality: {type: String,
        required:true
    },
    noext: {type: String,
        required:true
    },
    noint: {type: String,
        required:false
    },
    role: {type: String,
        required:true
    },
    active: {type: Boolean,
        required:true
    },
});

userSchema.pre<IUser>("save", async function(next) {
    const user = this;

    if (!user.isModified("password")) return next();

    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(user.password, salt);
    user.password = hash;
    user.active = true;
    next();
});

userSchema.methods.comparePassword = async function(
    password: string
): Promise<Boolean> {
    return await bcrypt.compare(password, this.password);
};

export default model<IUser>("User", userSchema);
