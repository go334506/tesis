import {Schema, model} from "mongoose";

const MedicalSchema = new Schema({
    idUser: {type: String, required:true, unique: true},
    tipeBlood: {type: String},
    principalDoctor: {type: String},
    numberDoctor: {type: String},
    donor:{type: Boolean, required:true},

    createdAt: { type: Date, default: Date.now},
    updatedAt: Date
})

export default model('Medical',MedicalSchema);
