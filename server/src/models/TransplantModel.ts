import {Schema, model} from "mongoose";

const TransplantSchema = new Schema({
    idUser: {type: String, required:true},
    transplant: {type: String},
    date: {type: String}, // dia, mes y año
    cares: {type: String},

    createdAt: { type: Date, default: Date.now},
    updatedAt: Date
})

export default model('Transplant',TransplantSchema);
