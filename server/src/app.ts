
import express from 'express'
import passport from 'passport'
import passportMiddleware from './middlewares/passport';
import cors from 'cors';
import morgan from 'morgan';
import multer from 'multer';
import path from "path";
import { v4 as uuidv4 } from 'uuid';

import authRoutes from './routes/auth.routes';
import specialRoutes from './routes/special.routes';
import contactRoutes from './routes/contactRoutes';
import driverRoutes from './routes/driverRoutes';
import adminRoutes from './routes/adminRoutes';
import medicalRoutes from './routes/medicalRoutes';
import surgeryRoutes from './routes/surgeryRoutes';
import transplantRoutes from './routes/transplantRoutes';
import diseaseRoutes from './routes/diseaseRoutes';
import reportRoutes from './routes/reportRoutes';
import vehicleRoutes from './routes/vehicleRoutes';


const app = express();

const storage = multer.diskStorage({
    destination: path.join(__dirname,'public/images'),
    filename:(req,file,cb) => {
        cb(null, uuidv4() + path.extname(file.originalname))
    }
});

const upload = multer({
    storage,
    dest: path.join(__dirname,'public/images'),
    fileFilter:(req,file,cb) => {
        const fileTypes = /jpeg|jpg|png|gif/
        const mimetype = fileTypes.test(file.mimetype);
        const extname = fileTypes.test(path.extname(file.originalname))

        if(mimetype && extname){
            return cb(null, true)
        }

        // @ts-ignore
        cb('Error: El archivo debe ser una imagen ')

    }
}).single('image');

// settings
app.set('port', process.env.PORT || 3000);

// middlewares
app.use(morgan('dev'));
app.use(cors());
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(express.static(path.join(__dirname,'public')))
app.use(upload);


app.use(passport.initialize());
passport.use(passportMiddleware);


app.get('/', (req, res) => {
    return res.send(`The API is at http://localhost:${app.get('port')}`);
})

app.use('/api',authRoutes);

///rutas que necesitan autorización
app.use('/api',specialRoutes);
app.use('/api/driver/',driverRoutes);
app.use('/api/admin/',adminRoutes);
app.use('/api/report/',reportRoutes);
app.use('/api/medical/',medicalRoutes);
app.use('/api/surgery/',surgeryRoutes);
app.use('/api/transplant/',transplantRoutes);
app.use('/api/disease/',diseaseRoutes);
app.use('/api/vehicle/',vehicleRoutes);
app.use('/api/contact/',contactRoutes);


export default app;
