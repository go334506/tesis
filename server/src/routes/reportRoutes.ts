import {Router} from "express";
import {reportController} from '../controllers/reportController'
import passport from "passport";

class ReportRoutes {
    public router: Router = Router();
    constructor() {
        this.config();
    }
    config():void{
        this.router.post('/create/',passport.authenticate("jwt", { session: false }),reportController.createReport);
        this.router.get('/getreports/',passport.authenticate("jwt", { session: false }),reportController.getReports);
        this.router.get('/getreport/:id',passport.authenticate("jwt", { session: false }),reportController.getReport);
        this.router.put('/updatestatus/:id',passport.authenticate("jwt", { session: false }),reportController.updateStatusReport);

    }
}

const  reportRoutes = new ReportRoutes();
export default reportRoutes.router;
