import {Router} from "express";
import {contactController} from '../controllers/contactController'
import passport from "passport";

class ContactRoutes {
    public router: Router = Router();
    constructor() {
        this.config();
    }
    config():void{
        this.router.get('/getcontacts/',passport.authenticate("jwt", { session: false }),contactController.getContacts);
        this.router.post('/create/',passport.authenticate("jwt", { session: false }),contactController.createContact);
        this.router.get('/getcontact/:id',passport.authenticate("jwt", { session: false }),contactController.getContact);
        this.router.put('/edit/:id',passport.authenticate("jwt", { session: false }),contactController.editContact);
        this.router.delete('/delete/:id',passport.authenticate("jwt", { session: false }),contactController.deleteContact);

    }
}

const  contactRoutes = new ContactRoutes();
export default contactRoutes.router;
