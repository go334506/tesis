import {Router} from "express";
import {adminController} from '../controllers/adminController';
import passport from "passport";

class AdminRoutes {
    public router: Router = Router();
    constructor() {
        this.config();
    }
    config():void{
        this.router.get('/getdrivers/', passport.authenticate("jwt", { session: false }), adminController.getDrivers);

        this.router.get('/getdriver/:id',passport.authenticate("jwt", { session: false }), adminController.getDriverId);

        this.router.put('/edit/:id',passport.authenticate("jwt", { session: false }), adminController.editDriverId);

        this.router.delete('/delete/:id',passport.authenticate("jwt", { session: false }), adminController.deleteDriverId);

    }
}

const  adminRoutes = new AdminRoutes();
export default adminRoutes.router;
