import {Router} from "express";
import {diseaseController} from '../controllers/diseaseController'
import passport from "passport";


class DiseaseRoutes {
    public router: Router = Router();
    constructor() {
        this.config();
    }
    config():void{
        this.router.post('/create/',passport.authenticate("jwt", { session: false }),diseaseController.createDisease );
        this.router.put('/edit/:id',passport.authenticate("jwt", { session: false }),diseaseController.editDisease);
        this.router.get('/get/:id',passport.authenticate("jwt", { session: false }),diseaseController.getDisease );
        this.router.get('/get/',passport.authenticate("jwt", { session: false }),diseaseController.getDiseases );
        this.router.delete('/delete/:id',passport.authenticate("jwt", { session: false }),diseaseController.deleteDisease);

    }
}

const  diseaseRoutes = new DiseaseRoutes();
export default diseaseRoutes.router;
