import {Router} from "express";
import {surgeryController} from '../controllers/surgeryController'
import passport from "passport";
import {contactController} from "../controllers/contactController";

class SurgeryRoutes {
    public router: Router = Router();
    constructor() {
        this.config();
    }
    config():void{
        this.router.post('/create/',passport.authenticate("jwt", { session: false }),surgeryController.createSurgery );
         this.router.put('/edit/:id',passport.authenticate("jwt", { session: false }),surgeryController.editSurgeries);
        this.router.get('/getsurgery/:id',passport.authenticate("jwt", { session: false }),surgeryController.getSurgery );
        this.router.get('/surgeries/',passport.authenticate("jwt", { session: false }),surgeryController.getSurgeries );
        this.router.delete('/delete/:id',passport.authenticate("jwt", { session: false }),surgeryController.deleteSurgery);

    }
}

const  surgeryRoutes = new SurgeryRoutes();
export default surgeryRoutes.router;
