import {Router} from "express";
import {driverController} from '../controllers/driverController'
import passport from "passport";

class DriverRoutes {
    public router: Router = Router();
    constructor() {
        this.config();
    }
    config():void{
       //obtiene su id mediante el token del usuario
        this.router.get('/getdriver/',passport.authenticate("jwt", { session: false }), driverController.getDriver);

        this.router.put('/edit/',passport.authenticate("jwt", { session: false }), driverController.editDriver);

        this.router.delete('/delete/',passport.authenticate("jwt", { session: false }), driverController.deleteDriver);

    }
}

const  driverRoutes = new DriverRoutes();
export default driverRoutes.router;
