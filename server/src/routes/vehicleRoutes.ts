import {Router} from "express";
import {vehicleController} from  '../controllers/vehicleController';
import passport from "passport";

class VehicleRoutes {
    public router: Router = Router();
    constructor() {
        this.config();
    }
    config():void{
        this.router.post('/create/',passport.authenticate("jwt", { session: false }), vehicleController.createVehicle);
        this.router.put('/edit/:id',passport.authenticate("jwt", { session: false }),vehicleController.editVehicle);
        this.router.get('/getvehicle/:id',passport.authenticate("jwt", { session: false }), vehicleController.getVehicle);
        this.router.get('/getvehicles/',passport.authenticate("jwt", { session: false }),vehicleController.getVehicles);
        this.router.delete('/delete/:id',passport.authenticate("jwt", { session: false }), vehicleController.deleteVehicle);

        // fotos
        this.router.post('/uploadfoto/',passport.authenticate("jwt", { session: false }), vehicleController.uploadFotosVehicle);
        this.router.get('/getfotos/:id',passport.authenticate("jwt", { session: false }),vehicleController.getFotosVehicle);
        this.router.delete('/deletefotos/:id',passport.authenticate("jwt", { session: false }), vehicleController.deleteFotosVehicle);

    }
}

const  vehicleRoutes = new VehicleRoutes();
export default vehicleRoutes.router;
