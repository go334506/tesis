import {Router} from "express";
import {transplantController} from '../controllers/transplantController'
import passport from "passport";


class TransplantRoutes {
    public router: Router = Router();
    constructor() {
        this.config();
    }
    config():void{
        this.router.post('/create/',passport.authenticate("jwt", { session: false }),transplantController.createTransplant );
         this.router.put('/edit/:id',passport.authenticate("jwt", { session: false }),transplantController.editTransplant);
        this.router.get('/get/:id',passport.authenticate("jwt", { session: false }),transplantController.getTransplant );
        this.router.get('/get/',passport.authenticate("jwt", { session: false }),transplantController.getTransplants );
        this.router.delete('/delete/:id',passport.authenticate("jwt", { session: false }),transplantController.deleteTransplant);

    }
}

const  transplantRoutes = new TransplantRoutes();
export default transplantRoutes.router;
