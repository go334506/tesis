import {Router} from "express";
import {medicalController} from '../controllers/medicalController'
import passport from "passport";

class MedicalRoutes {
    public router: Router = Router();
    constructor() {
        this.config();
    }
    config():void{
        this.router.post('/create/',passport.authenticate("jwt", { session: false }),medicalController.createMedical );
        this.router.put('/edit/',passport.authenticate("jwt", { session: false }),medicalController.editMedical);
        this.router.get('/getmedical/',passport.authenticate("jwt", { session: false }),medicalController.getMedical );
        this.router.get('/getmedicals/',passport.authenticate("jwt", { session: false }),medicalController.getMedicals );

    }
}

const  medicalRoutes = new MedicalRoutes();
export default medicalRoutes.router;
