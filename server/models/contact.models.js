const mongoose = require ('mongoose');
const {Schema} = mongoose;

const ContactSchema = new Schema({
    idDriver: {type: String, required:true},
    name: {type: String},
    appat: {type: String},
    amat: {type: String},
    phone: {type: String},
    email: {type: String},
    cp: {type: Number},
    calle: {type: String},
    colony: {type: String},
    municipality: {type: String},
    noext: {type: Number},
    noint: {type: Number}


});

module.exports= mongoose.model('Contact', ContactSchema);
