const mongoose = require ('mongoose');
const {Schema} = mongoose;


const ReportSchema = new Schema({
    idDriver: {type: String, required:true},
    isoDate: {type: Date, required:true},//falta fecha y hora
    date: {type: String, required:true},
    hour: {type: String, required:true},
    velocity: {type: Number, required:true},
    metricUnit: {type: String, required:true},
    latitud: {type: Number, required:true},
    longitud: {type: Number, required:true},
    x: {type: String, required:true},
    y: {type: String, required:true},
    z: {type: String, required:true},
    statusReport: {type: String, required:true},//status del reporte


});

module.exports= mongoose.model('Report', ReportSchema);
