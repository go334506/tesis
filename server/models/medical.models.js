const mongoose = require ('mongoose');
const {Schema} = mongoose;

const MedicalSchema = new Schema({
    idDriver: {type: String, required:true},
    tipeBlood: {type: String},
    disease: {type: String},
    treatment: {type: String},
    surgeries: {type: String},
    transplant: {type: String},
    donor:{type: Boolean, required:true},
});

module.exports= mongoose.model('Medical', MedicalSchema);
