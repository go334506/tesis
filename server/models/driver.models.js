const mongoose = require ('mongoose');
const {Schema} = mongoose;

const DriverSchema = new Schema({

    user: {type: String, required:true},
    name: {type: String, required:true},
    appat: {type: String, required:true},
    amat: {type: String, required:false},
    phone: {type: String, required:true},
    email: {type: String, required:true},
    edad: {type: Number, required:true},
    cp: {type: Number, required:true},
    calle: {type: String, required:true},
    colony: {type: String, required:true},
    municipality: {type: String, required:true},
    noext: {type: Number, required:true},
    noint: {type: Number, required:false}


});

module.exports= mongoose.model('Driver', DriverSchema);
