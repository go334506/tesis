const mongoose = require ('mongoose');
const {Schema} = mongoose;
//recurda que hay datos que son necesario pedirlos
const VehicleSchema = new Schema({
    idDriver: {type: String},
    enrollment: {type: String},//Matrícula
    brand: {type: String},
    model: {type: String},
    color: {type: String},
    year: {type: Number},
    country: {type: String},
    nEngine: {type: String},
    nChassis: {type: String},
});

module.exports= mongoose.model('Vehicle', VehicleSchema);
